# 介绍

<center><img src="/django-vue-admin.png" width="200" height="200"></center>

<br>

<center><h2>Django-Vue-Admin</h2>
</center>

<p style="display: flex;
    justify-content: center;
    align-items: center;
    ">
    <img src="https://img.shields.io/badge/license-MIT-blue.svg">&nbsp;&nbsp;
    <img src="https://img.shields.io/badge/python-%3E=3.9.x-green.svg">&nbsp;&nbsp;
    <img src="https://img.shields.io/badge/django%20versions-4.0-blue">&nbsp;&nbsp;
    <img src="https://img.shields.io/badge/node-%3E%3D%2016.0.0-brightgreen"/>&nbsp;&nbsp;
    <img src="https://gitee.com/liqianglog/django-vue-admin/badge/star.svg?theme=dark"/>&nbsp;&nbsp;
</p>

::: tip 缘起
一直想做一款后台管理系统，看了很多优秀的开源项目，发现只有Java、Go，但是发现没有合适Python的。于是利用空闲休息时间开始自己写一 套后台系统。如此有了Django-Vue-Admin管理系统。您可以基于此系统进行持续开发，以做出符合自己系统。所有前端后台代码封装过后十分精简易上手， 出错概率低。同时支持移动客户端访问。系统会陆续更新一些实用功能。
:::

#### 在线体验
演示地址：[http://demo.django-vue-admin.com](http://demo.django-vue-admin.com/)

账号：superadmin 密码：admin123456

文档地址：[http://django-vue-admin.com](http://django-vue-admin.com/)

前端FastCrud配置文档地址：[http://fast-crud.docmirror.cn/](http://fast-crud.docmirror.cn/)

后端Django文档地址：[https://docs.djangoproject.com/en/4.1/](https://docs.djangoproject.com/en/4.1/)

插件市场：[https://bbs.django-vue-admin.com/plugMarket.html](https://bbs.django-vue-admin.com/plugMarket.html)

#### 交流群

- QQ群号：群1: 812482043[点击加入群聊](https://jq.qq.com/?_wv=1027&k=OqvIlq7n)&nbsp; 群2: 687252418[点击加入群聊](https://jq.qq.com/?_wv=1027&k=Kk4y1LHR)

- 二维码 

<div style="float:left;border:solid 1px 000;margin:20px;"><img src="/qq1.jpeg"  width="200" height="260" ></div>

<div style="float:left;border:solid 1px 000;margin:20px;"><img src="/qq2.jpeg" width="200" height="260" ></div>

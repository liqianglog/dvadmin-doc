# 项目介绍
## 整体结构
~~~
├── web/                    // 前端项目
├── backend/                // 后端项目
├── docker_env/             // docker 安装配置
├── docker-compose.yml      // docker-compose 配置
├── README.md               // 项目的说明文档
├── LICENSE                 // 开源 LICENSE
└── README.md               // 说明文档
~~~

### web前端项目结构
~~~
├── README.md               // 项目的说明文档
├── dist                    // build 后存放内容
├── package.json            // npm包配置文件，里面定义了项目的npm脚本，依赖包等信息
├── babel.config.js         // babel
├── public                  // 项目根目录
│   ├── image/              // 主题展示目录
│   ├── icon.ico            // ico 图标
│   └── index.html          // 首页入口文件，你可以添加一些 meta 信息或统计代码啥的。
├── src                     // 开发的目录
│   ├── App.vue             // 项目入口文件
│   ├── api                 // 系统公告 api 接口
│   ├── assets              // 项目公用资源目录（图片，ico）
│   ├── components          // 公共组件
│   ├── config              // 基本配置
│   ├── layout              // 全局 layout
│   ├── libs                // 全局公用方法
│   ├── locales             // 国际化
│   ├── menu                // 菜单权限处理
│   ├── plugin              // d2admin 前端插件
│   ├── router              // 路由
│   ├── store               // 全局 store管理
│   └── views               // views 所有页面
│       ├── dashboard       // 首页内容
│       ├── demo            // demo 示例
│       ├── plugins         // dvadmin 插件
│       ├── system          // dvadmin 系统专有视图，建议不要修改或新增内容
│       │   ├── ...
│       │   ├── login       // 登录页面
│       │   └── ...
│       ├── App.vue         // vue App 入口
│       ├── install.js      // d2admin 前端通用配置
│       ├── main.js         // vue main 入口
│       └── setting.js      // d2admin 配置
│   ├── main.js             // 项目的核心文件
│   ├── permission.js       // 页面是否登录判断权限判断，权限白名单
│   ├── settings.js         // 全局配置 
│   ├── .env.development    // 开发环境配置
│   ├── .env.staging        // 预发布环境配置
│   ├── .env.production     // 生产环境配置
│   └── ...
└── vue.config.js           // 本地跨域代理
~~~

### 后端项目结构
~~~
├── application                     // 工程名称
│   ├── asgi.py                     // Django asgi 默认配置
│   ├── celery.py                   // celery 默认配置
│   ├── settings.py                 // 项目的settings配置
│   ├── urls.py                     // 项目主URL对应关系
│   └── wsgi.py                     // wsgi 默认配置
├── conf                            // 配置信息
│   ├── env.example.py              // 配置信息模板
│   └── env.py                      // 自行根据 env.example.py 复制重命名为env.py ，系统只会去读取env.py配置
├── dvadmin                         // docker 启动 celery 脚本
│   ├── system                      // dvadmin 系统app
│   └── utils                       // 全局公用方法
├── logs                            // 日志存放位置
├── media                           // 上传的文件存放位置
├── plugins                         // dvadmin 插件目录
├── └── ...                         
│── static                          // 静态文件
│   ├── drf-yasg                    // 静态文件 drf-yasg 静态文件
│   ├── ...
│   └── rest_framework              // rest_framework 静态文件
├── docker_start.sh                 // docker 启动 django 脚本
├── manage.py
└── requirements.txt                 // 项目环境依赖

~~~

### Docker 目录
~~~
├── celery                              // 后端 celery 容器配置目录
│   └── Dockerfile                      // 后端 celery Dockerfile
├── django                              // 后端 django 容器配置目录
│   ├── DockerfileBuild                 // 基础镜像Build
│   └── Dockerfile                        // 后端 django Dockerfile
├── mysql                               // Mysql 数据库容器配置目录
│   ├── conf.d                          // 数据库配置
│   │   └── my.cnf                      // 数据库 my.cnf 配置
│   ├── data                            // 数据库数据
│   ├── launch.sh                           
│   └── logs                            // 数据库日志
├── redis                               // redis 容器配置目录
│   ├── data                            // redis数据
│   ├── launch.sh
│   └── redis.conf                      // redis 配置
└── web                                 // 前端 库容器配置目录
    ├── DockerfileBuild                 // 基础镜像Build
    └── Dockerfile                      // 前端 Dockerfile
~~~

## 配置文件
### 前端配置
开发环境配置路径 `web/.env.development`
~~~
# 开发环境
NODE_ENV=development
# 页面 title 前缀
VUE_APP_TITLE=DVAdmin
# 启用权限管理
VUE_APP_PM_ENABLED = true
# 后端接口地址及端口(域名)
VUE_APP_API = "http://127.0.0.1:8000"
# 部署路径
VUE_APP_PUBLIC_PATH=/
~~~

生产环境配置路径 `web/.env.preview`
~~~
# 指定构建模式
NODE_ENV=production
# 标记当前构建方式
VUE_APP_BUILD_MODE=PREVIEW
# 页面 title 前缀
VUE_APP_TITLE=DVAdmin
# 显示源码按钮
VUE_APP_SCOURCE_LINK=FALSE
# 部署路径
VUE_APP_PUBLIC_PATH=/
# 启用权限管理
VUE_APP_PM_ENABLED = true
~~~

### 后端配置 
配置路径 `backend/conf/env.example.py`
~~~
# ================================================= #
# ************** 数据库 配置  ************** #
# ================================================= #
#
# 数据库 ENGINE ，默认演示使用 sqlite3 数据库，正式环境建议使用 mysql 数据库
DATABASE_ENGINE = "django.db.backends.sqlite3"
# 数据库名
DATABASE_NAME = os.path.join(BASE_DIR, 'db.sqlite3')
# 数据库地址 改为自己数据库地址
DATABASE_HOST = "127.0.0.1"
# 数据库端口
DATABASE_PORT = 3306
# 数据库用户名
DATABASE_USER = "root"
# 数据库密码
DATABASE_PASSWORD = "123456"

# ================================================= #
# ************** redis配置，无redis 可不进行配置  ************** #
# ================================================= #
# REDIS_PASSWORD = ''
# REDIS_HOST = '127.0.0.1'
# REDIS_URL = f'redis://:{REDIS_PASSWORD or ""}@{REDIS_HOST}:6380'
# ================================================= #
# ************** 其他 配置  ************** #
# ================================================= #
DEBUG = True  # 线上环境请设置为True
ALLOWED_HOSTS = ["*"]
LOGIN_NO_CAPTCHA_AUTH = True  # 登录接口 /api/token/ 是否需要验证码认证，用于测试，正式环境建议取消
~~~

### Docker 配置
待完善...

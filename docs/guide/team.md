---
layout: page
---
<script setup>
import {
  VPTeamPage,
  VPTeamPageTitle,
  VPTeamMembers
} from 'vitepress/theme'

const members = [
  {
    avatar: 'https://bbs.django-vue-admin.com/uploads/20220417/c83ce311969d153e86f1c020002e452c.png',
    name: 'liqianglog',
    title: 'Creator 核心开发者',
    links: [
      { icon: 'github', link: 'https://github.com/yyx990803' },
      { icon: 'twitter', link: 'https://twitter.com/youyuxi' }
    ]
  },
  {
    avatar: 'https://foruda.gitee.com/avatar/1676983668265399615/1694793_yuanxiaotian_1578957929.png!avatar200',
    name: '猿小天',
    title: '核心开发者，后端django',
    links: [
      { icon: 'github', link: 'https://github.com/yyx990803' },
      { icon: 'twitter', link: 'https://twitter.com/youyuxi' }
    ]
  },
  {
    avatar: 'https://foruda.gitee.com/avatar/1676572432941046898/7947594_h0ngza1_1676572432.png!avatar200',
    name: 'H0nGzA1',
    title: '核心开发者 插件开发 论坛问答',
    links: [
      { icon: 'github', link: 'https://github.com/yyx990803' },
      { icon: 'twitter', link: 'https://twitter.com/youyuxi' }
    ]
  },
]
</script>

<VPTeamPage>
  <VPTeamPageTitle>
    <template #title>
      dvadmin开发组
    </template>
    <template #lead>
      我们是一群热爱代码的青年，在这个炙热的时代下，我们希望静下心来通过Code带来一点我们的色彩和颜色。
    </template>
  </VPTeamPageTitle>
  <VPTeamMembers
    :members="members"
  />
</VPTeamPage>

# 捐赠支持
::: tip 说明
如果你觉得这个项目帮助到了你，你可以请作者喝杯咖啡表示鼓励 ☕️
:::

| 支付宝        | 微信           |
| ------------- |:-------------:|
| <img height="350" width="300" alt="markdown" src="/alipay.jpg" title="支付宝">     | <img  height="350" width="300" alt="markdown" src="/wechat.jpg" title="微信"> |

::: tip 鸣谢
感谢所有为开源项目添柴助力的大大们
:::
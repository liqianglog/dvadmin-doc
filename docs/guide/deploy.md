
# 项目运行及部署
## 准备工作
~~~
Python >= 3.7.0 (推荐3.9+版本)
nodejs >= 14.0 (推荐最新)
Mysql >= 5.7.0 (可选，默认数据库sqlite3，推荐8.0版本)
Redis(可选，最新版)
~~~
* 基于[PyCharm工具开发(待更新)](#pycharm工具开发)
* 基于[VsCode工具开发(待更新)](#vscode工具开发)
* 基于[Docker环境开发(待更新)](#docker环境开发)

## 运行预览

### 下载代码

- 通过 `git clone https://gitee.com/liqianglog/django-vue-admin.git` 下载到工作目录

### 后端运行
1. 进入后端项目目录:`cd backend`
2. 在项目根目录中，复制 `./conf/env.example.py` 文件为一份新的到 `./conf/env.py` 下，并重命名为`env.py`
3. 在 `env.py` 中配置数据库信息(默认数据库为sqlite3，测试演示可忽略此步骤)
4. 安装依赖环境:
	`pip3 install -r requirements.txt`
5. 执行迁移命令:
	`python3 manage.py makemigrations`
	`python3 manage.py migrate`
6. 初始化数据:
	`python3 manage.py init`
7. 初始化省市县数据:
	`python3 manage.py init_area`
8. 启动项目:
	`python3 manage.py runserver 0.0.0.0:8000`
        
### 前端运行
1. 进入前端项目目录 `cd web`
2. 安装依赖 `npm install --registry=https://registry.npm.taobao.org`
3. 启动服务 `npm run dev`

### 访问项目
- 访问地址：[http://localhost:8080](http://localhost:8080) (默认为此地址，如有修改请按照配置文件)
- 账号：`superadmin` 密码：`admin123456`


# 环境部署
* 普通部署
* [docker-compose一键部署](#docker-compose一键部署)
* 宝塔部署(待更新)

## 普通部署
### 前端构建
1. 修改环境配置 `.env.preview` 文件中，<b>`VUE_APP_BASE_API` 地址为后端服务器地址</b>

<img src="/_images/01.png"  height="400" width="600" style="margin-left: 40px;">

2. 构建生产环境 `npm run build`，等待生成 `dist` 文件夹
3. 生成的 `dist` 文件放到服务器 `/opt/django-vue-admin/web/dist` 目录下
4. 使用 [nginx](#nginx配置) 进行静态文件代理

### 后端启动
1. 使用daphne启动：`daphne -b 0.0.0.0 -p 8000 application.asgi:application`
2. 后台运行： `nohup daphne -b 0.0.0.0 -p 8000 application.asgi:application > run.log 2>&1 &`

### Nginx配置

~~~
# 此nginx配置默认项目放在服务器的 /opt 目录下，前端默认端口为 8080，后端为8000，如需修改请自行对照修改

events {
    worker_connections  1024;
}
http {
    include       mime.types;
    default_type  application/octet-stream;
    sendfile        on;

	server {
        listen 80 default_server;
        server_name _;
        return 404;
        }
    # 前端配置
    server {
        listen       8080;
        client_max_body_size 100M;
        server_name  dvadmin-web;
        charset utf-8;
        location / {
            proxy_set_header Host $http_host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header X-Forwarded-Proto https;
            root /opt/django-vue-admin/web/dist;
            index  index.html index.php index.htm;
        }
        # 后端服务
        location /api {
          root html/www;
          proxy_set_header Host $http_host;
          proxy_set_header  X-Real-IP $remote_addr;
          proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
          proxy_set_header X-Forwarded-Proto $scheme;
          rewrite ^/api/(.*)$ /$1 break;  #重写
          proxy_pass http://127.0.0.1:8000;
    
        }
        # Django media
        location /media  {
            root /opt/django-vue-admin/backend;  # your Django project's media files - amend as required
        }
        # Django static
        location /static {
            root /opt/django-vue-admin/backend; # your Django project's static files - amend as required
        }
        #禁止访问的文件或目录
        location ~ ^/(\.user.ini|\.htaccess|\.git|\.svn|\.project|LICENSE|README.md)
        {
            return 404;
        }
    }
}

~~~
### 访问项目
- 访问地址：`http://公网或本服务器ip:8080` (服务器需注意供应商端防火墙是否开启对应端口)
- 账号：`superadmin` 密码：`admin123456`

## docker-compose一键部署
### 构建
~~~
# 先安装docker-compose (自行百度安装)
# 通过 `git clone https://gitee.com/liqianglog/django-vue-admin.git` 下载到工作目录，执行此命令等待安装
docker-compose up

# 初始化后端数据(第一次执行即可)
docker exec -ti DVAdmin-django bash
python manage.py makemigrations 
python manage.py migrate
python manage.py init -y
python manage.py init_area
exit
~~~

### 访问项目
- 访问地址：[http://localhost:8000](http://localhost:8000) 
- 账号：`superadmin` 密码：`admin123456`

### 常用命令
~~~
# 使用docker-compose启动四个容器
docker-compose up

# 如果前端代码有更新,可以使用此命令重新打包镜像
docker-compose build DVAdmin-web

# 使用docker-compose 后台启动
docker-compose up -d

# 服务都启动成功后,使用此命令行可清除none镜像
docker system prune

# 查询容器运行日志
前端日志：  docker logs DVAdmin-ui -f --tail 100
后端日志：  docker logs DVAdmin-django -f --tail 100
~~~

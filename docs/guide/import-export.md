## 序列化器
```
class UserImportSerializer(CustomModelSerializer):  
    status_label=serializers.CharField(source='get_status_display', read_only=True,  
                                                   help_text='状态')   
    dept_name = serializers.CharField(source='dept.name', read_only=True, default=None, help_text='部门名称')  
    create_datetime = serializers.DateField(required=False, help_text="创建时间")

	class Meta:  
    model = Users  
    fields = "__all__"  
    read_only_fields = ["id"]
```
## 导入配置
```
import_field_dict = {  
    "status": {  
        "title": "状态",  
        "display": "status_label",  
        "choices": {  
            "data": {"启用": 1, "禁用": 0},  
         }    
        }
    "name": "名称",   
    'create_datetime': {  
        "title": '创建时间',  
        "display": 'create_datetime',  
        "type": "date"  
    },  
    "dept": {  
        "title": "部门",  
        "display": 'dept_name',  
        "choices": {  
            "queryset": Dept.objects.values('name'),  
            "values_name": "name"  
        }  
    },
    "description": "备注"
}
```
## 导入配置说明
|字段|字段说明|特别注意|
|--|--|--|
|import_field_dict|配置导入的字典|无|
|status|字典的键,对应数据库的字段|根据您自己的字段来设置|
|title|excle表格中显示的头部信息|批量更新时需要这样使用|
|display|excle表中显示的内容|批量更新时需要这样使用|
|choices|枚举设置|批量更新时需要这样使用|
|data|枚举设置|批量更新时需要这样使用|
|queryset|关联的ORM|批量更新时需要这样使用|
|values_name|关联ORM中的字段名,用于匹配批量更新|批量更新时需要这样使用|
## 导出配置
```
export_field_label = {  
    "status_label": "状态",
    "name": "名称",   
    'create_datetime': "创建时间",  
    "dept_name": "部门",
    "description": "备注"
}
```
## 导出配置说明
|字段|字段说明|特别注意|
|--|--|--|
|export_field_label|配置导出的字典|无|
|status_label|字典的键|由于是枚举,所以由序列化器处理后返回|

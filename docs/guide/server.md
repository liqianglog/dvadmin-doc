# 后台手册

### 环境部署教程
- 环境搭建视频教程上：https://www.bilibili.com/video/BV15T4y1Y7ug
- 环境搭建视频教程下：https://www.bilibili.com/video/BV1Cr4y1p7UC
- Windows 使用Anaconda3搭建dvadmin：https://bbs.django-vue-admin.com/article/3.html

### 二次开发教程
- 快速CRUD开发教程：https://bbs.django-vue-admin.com/article/9.html
- 如何在 env.py 文件配置Mysql数据库：https://bbs.django-vue-admin.com/question/4.html
- 导入导出配置教程：https://bbs.django-vue-admin.com/article/10.html
- 后端插件开发教程：https://bbs.django-vue-admin.com/article/4.html

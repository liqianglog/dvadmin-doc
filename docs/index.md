---
layout: home
title: dvadmin3

hero:
  name: Django-vue3-admin
  text: 快速开发全栈管理系统
  tagline: 基于Django + Vue搭建的快速开发平台.
  image:
    src: /django-vue-admin.png
  actions:
    - theme: brand
      text: Get Started
      link: /guide/getting-started
    - theme: alt
      text: 插件市场🚀
      link: https://bbs.django-vue-admin.com/plugMarket.html
    - theme: alt
      text: View on Gitee
      link: https://gitee.com/huge-dream/django-vue3-admin
    - theme: alt
      text: View on GitHub
      link: https://gitee.com/huge-dream/django-vue3-admin
    


features:
- icon: 🔫
  title: 简单易用 
  details: 大幅度降低应用层代码难度，让每一个刚开始学习 django和vue的新手都能快速上手。这将会是你上手学习 django+vue的最佳代码。
- icon: 🔧
  title: 自由拓展 
  details: 插件市场众多插件，满足你的业务需求，系统底层代码和业务逻辑代码分层清晰，不会发生相互干扰，便于根据自己业务方向进行拓展。
- icon: 📇
  title: 标准化目录 
  details: 项目目录分层清晰，项目模式结构清晰，包名语义化，让你更加容易理解目录结构，读懂代码更加方便！
- icon: 🧰
  title: 功能完善  
  details: 内置完整的权限架构，包括：菜单、角色、用户、字典、参数、监控、代码生成等一系列系统常规模块。
- icon: 💻
  title: 可视化表单代码生成
  details: 无需手写任何代码，拖拽即可生成业务菜单，一键生成模块，包含增删改查/排序/导出/权限控制等操作，安装即可使用。
- icon: 🚪
  title: 完全响应式布局 
  details: 提供多终端适配：电脑、平板、手机等所有主流设备，提供多种不同风格的皮肤。页面美观，高端大气上档次。
---
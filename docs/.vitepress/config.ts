import { defineConfig } from "vitepress";

export default defineConfig({
  lang: "en-US",
  title: "Django-vue3-admin",
  ignoreDeadLinks: true,
  description: "基于 Django+vue 搭建的快速开发全栈管理系统",
  appearance: true,
  lastUpdated: true,
  cleanUrls: true,
  base: "/",
  themeConfig: {
    logo: "/logo.ico",
    siteTitle: "dvadmin3",
    nav: [
      {
        text: "开发教程",
        items: [
          { text: "视频教程", link: "https://www.bilibili.com/video/BV1xF41137tC/?vd_source=d7f935829da41c02d0e73c21983041c5" },
          { text: "部署教程", link: "https://www.bilibili.com/video/BV1ex4y1F7iH/?vd_source=d7f935829da41c02d0e73c21983041c5" },
        ],
      },
      {
        text: "🛒体验项目",
        items: [
          { text: "Dvadmin2(Vue2版本)", link: "/item-1" },
          { text: "Dvadmin3(Vue3版本)", link: "/item-3" },
        ],
      },
      { text: "🚀插件市场", link: "https://bbs.django-vue-admin.com/plugMarket.html" },
      { text: "项目成员", link: "/guide/team" },
      {
        text: "战略合作",
        items: [
          { text: "GreaterWMS(开源仓库管理软件)", link: "https://www.56yhz.com/" },
          { text: "SimpleUi Django admin", link: "https://simpleui.72wo.com/" },
          { text: "gin-vue-admin", link: "https://www.gin-vue-admin.com/" },
          { text: "LaravelAdmin", link: "https://www.laraveladmin.cn/open/index" },
          { text: "spug(轻量级自动化运维平台)", link: "https://spug.cc/" },
          { text: "Vform(低代码表单)", link: "https://www.vform666.com/" },
        ],
      },
    ],
    sidebar: [
      {
        text: "指南",
        items: [
          { text: "快速了解", link: "/guide/quick-introduction" },
          { text: "项目运行及部署", link: "/guide/deploy" },
          { text: "项目介绍", link: "/guide/project-struct" },
          { text: "前端手册", link: "/guide/web" },
          { text: "后端手册", link: "/guide/server" },
          { text: "导入导出配置", link: "/guide/import-export" },
          { text: "更新日志", link: "/guide/CHANGELOG" },
        ],
      },
      {
        text: "其他",
        items: [
          { text: "常见问题", link: "/guide/question" },
          { text: "捐赠支持", link: "/guide/coffee" },
        ],
      },
    ],
    socialLinks: [
      {
        icon: {
          svg: '<?xml version="1.0" standalone="no"?><!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd"><svg t="1676966626128" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="1526" xmlns:xlink="http://www.w3.org/1999/xlink" width="200" height="200"><path d="M512 1024C230.4 1024 0 793.6 0 512S230.4 0 512 0s512 230.4 512 512-230.4 512-512 512z m259.2-569.6H480c-12.8 0-25.6 12.8-25.6 25.6v64c0 12.8 12.8 25.6 25.6 25.6h176c12.8 0 25.6 12.8 25.6 25.6v12.8c0 41.6-35.2 76.8-76.8 76.8h-240c-12.8 0-25.6-12.8-25.6-25.6V416c0-41.6 35.2-76.8 76.8-76.8h355.2c12.8 0 25.6-12.8 25.6-25.6v-64c0-12.8-12.8-25.6-25.6-25.6H416c-105.6 0-188.8 86.4-188.8 188.8V768c0 12.8 12.8 25.6 25.6 25.6h374.4c92.8 0 169.6-76.8 169.6-169.6v-144c0-12.8-12.8-25.6-25.6-25.6z" fill="#888888" p-id="1527"></path></svg>',
        },
        link: "https://gitee.com/huge-dream/django-vue3-admin",
      },
      { icon: "github", link: "https://gitee.com/huge-dream/django-vue3-admin" },
    ],
    footer: {
      message: "MIT Licensed | Copyright © 2021-2021 django-vue-admin All Rights Reserved",
      copyright: "晋ICP备18005113号-3",
    },
  },
});
